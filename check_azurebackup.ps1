<#
.SYNOPSIS
This script will pull data from the MARS (Microsoft Azure Recovery Service) agent
and then output information and performance data that can be used by icinga2
and other monitoring solutions that relies on console output in the form of text|perfdata

.DESCRIPTION
  Script used to output easily readable and 
  usable status information from the azure backup agent

.PARAMETER <Parameter_Name>
  None

.INPUTS
  None
.OUTPUTS
  Outputs information in the format  Readable string|Performance Data

.NOTES
  Version:        1.1
  Author:         Hampus Pettersson
                  hampus@hempux.net
  
  Creation Date:  2018-07-20
  Purpose/Change: Add performance data
  
#>

## create the $output string so can it can be used later.
[string]$outputtext

## $perfdata is defined so it can be used at the end of the script
$perfdata = New-Object PSObject -Property @{            
        Datasize       = ""                 
        Duration       = ""              
        }



If (Get-OBJob) {
$currentBackupjob = Get-OBJob

Switch ($currentBackupjob.JobStatus.JobState)
       {
       "EstimatingSize"   {$outputtext = "Preparing backup(Estimating datasize)";    $Exitcode = 0}
      
       "TransferringData" {$outputtext = "Data is being transferred";                $Exitcode = 0}

       "VerifyingBackup"  {$outputtext = "Backup completed, Data is being verified"; $Exitcode = 0}


        }



# Check how long the backup has been running for and warn if its been running too long without finishing.


[timespan]$runtime = (get-date).Subtract($backup.DisplayTime)

If($runtime.Hour -gt -2 ){
$outputtext = "WARN: Backup has been running for "+$runtime.Hour+" Hours"
$Exitcode = 1
}

}


## If the backup is not running do this instead


Else 
{
## Create summary for latest backup
        
$Backup   = Get-OBJob -previous 1

## Gather the time it took to back up data and also generate text for the console output.
$Duration = $backup.jobstatus.EndTime - $backup.jobstatus.StartTime
$Durationtotext = $Duration.Hours.ToString()+" hour(s) and "+$Duration.Minutes.ToString()+" minute(s)."

## Get the jobstate (Finished,error,Pending etc)
$Jobstate = $Backup.JobStatus.JobState


## Create output info based on the jobstatus
Switch ($Jobstate)
      {
      "Completed"             {$outputtext = "Previous backup completed in $Durationtotext";    $Exitcode = 0}
      
	  "CompletedWithWarning"  {$outputtext = "Previous backup completed with warning(s)";       $Exitcode = 1}
	  
      "Aborted"               {$outputtext = "Backup was aborted";                              $Exitcode = 1}
      
      "Failed"                {$outputtext = "Latest backup failed";                            $Exitcode = 2}
      }


## How long has it been since last backup?
$previoustbackuptime = (get-date).Subtract($backup.JobStatus.EndTime)


## Generate warnings based on time since last backup if it has been more than 30 hours.
Switch ($previoustbackuptime)
{        
       "$previoustbackuptime -lt -30" {$outputtext = "Last backup was more than 30 hours ago"; $Exitcode = 1 }        
       
       "$previoustbackuptime -lt -48" {$outputtext = "Last backup was $previoustbackuptime.hours hours ago"; $Exitcode = 2 }
       
} 



} # End of the competed backup stuff

### Perfdatastuff

[string]$perfdata.Duration =  [math]::round($Duration.TotalSeconds).tostring()+"s"
[string]$perfdata.Datasize = ($Backup.JobStatus.DatasourceStatus.ByteProgress.Progress / 1GB).ToString()+"GB"


## Add status prefix based on exitcode
switch ($Exitcode)
{
    0 {$outputtext = "OK: "  +$outputtext}
    1 {$outputtext = "WARN: "+$outputtext}
    2 {$outputtext = "CRIT: "+$outputtext}
}

##format perfdata before outputting it.
$perfinfo = "Time="+$perfdata.Duration+" Data="+($perfdata.Datasize).Replace("," , ".") 

Write-output "$outputtext|$perfinfo"
Exit $Exitcode